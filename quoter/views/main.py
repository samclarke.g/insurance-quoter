import json
from flask import Blueprint, render_template, request, redirect, g, url_for
from flask_login import current_user
from .. import app, db
from ..decorators import check_confirmed
from ..models import Driver, VehicleForm, Vehicle, HomeForm


main = Blueprint('main', __name__)


def save_driver(driver):
    driver = Driver(
        full_name=driver['fullName'],
        drivers_license=driver['driversLicense'],
        mailing_address=driver['mailingAddress'],
        date_of_birth=driver['dateOfBirth'],
        tickets=driver['tickets']
    )
    return driver


def save_vehicle(vehicle):
    vehicle = Vehicle(
        vin=vehicle['vin'],
        year=vehicle['year'],
        make=vehicle['make'],
        model=vehicle['model'],
        coverage=vehicle['coverage'],
        use=vehicle['use']
    )
    return vehicle



def save_vehicle_form(form, user, driver):
    print('vehicle form {}'.format(form))
    vehicle_form = VehicleForm(
        user=user,
        primary_driver=driver,
        college_grads=form['collegeGrads'],
        college_grads_details=form['collegeGradsDetails'],
        medical_coverage=form['medicalCoverage'],
        current_billing=form['currentBilling'],
        coverage_required=form['coverage'],
        use=form['use'],
    )
    for vehicle in form['vehicles']:
        v = save_vehicle(vehicle=vehicle)
        vehicle_form.vehicles.append(v)
    for additional_driver in form['additionalDrivers']:
        d = save_driver(driver=additional_driver)
        vehicle_form.additional_drivers.append(d)
    return vehicle_form


def save_home_form(form, user):
    home_form = HomeForm(
        user=user,
        full_name=form['fullName'],
        age=form['age'],
        basement_exists=form['basementExists'],
        basement_percent_finished=form['basementPercentFinished'],
        recent_loss=form['recentLoss'],
        wood_stove=form['woodStove'],
        current_billing=form['currentBilling']
    )
    return home_form

# -------- ROUTES -------------------------------------
@check_confirmed
@app.route('/', methods=['GET', 'POST'])
def index():
    """Index."""
    if not g.user.is_authenticated:
        return redirect(url_for('auth.login'))
    return render_template('index.html')


@app.route('/success', methods=['GET'])
def success():
    """Success."""
    return 'SUCCESS!'



@app.route('/save', methods=['GET', 'POST'])
def save():
    """Save Form."""
    form = None
    if request.method == 'POST' and request.data:
        form = json.loads(request.data)['data']
        # debug ----
        print('user: {}'.format(current_user))

        #  save vehicle data
        if form['type'] == 'vehicle':    
            driver = save_driver(
                driver=form['primaryDriver']
            )
            vehicle_form = save_vehicle_form(
                form=form,
                user=current_user,
                driver=driver
            )
            db.session.add(driver)
            db.session.add(vehicle_form)
            db.session.commit()
        
        #  save home data
        if form['type'] == 'home':
            home_form = save_home_form(
                form=form,
                user=current_user
            )
            db.session.add(home_form)
            db.session.commit()

        #  on success redirect to a success page
        return redirect(url_for('success'))
    return json.dumps(form)