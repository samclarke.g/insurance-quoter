import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail



def import_modules():
    from . import views
    from . import models
    from . import logs
    from . import admin


############
# APP INIT #
############
app = Flask(__name__, static_folder = '../dist')
#  load settings from Config class,
#  depending on environmental variable
app.config.from_object(os.environ['APP_SETTINGS'])
db = SQLAlchemy(app)
mail = Mail(app)


# import into module namespace
import_modules()
