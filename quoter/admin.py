from flask import g
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from . import app, db
from . models import User, VehicleForm, Vehicle, HomeForm, Driver


# set optional bootswatch theme
app.config['FLASK_ADMIN_SWATCH'] = 'cosmo'

admin = Admin(app, name='Insurance Quoter', template_mode='bootstrap3')

# Add administrative views here
class UserAdmin(ModelView):
    column_searchable_list = ('username', User.username)
    column_list = (
        'username', 'email', 'registered_on', 'role',
    )
    # Prevent administration of Users unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return hasattr(g.user, 'role') and g.user.role == 'admin'

class DriverAdmin(ModelView):
    # Prevent administration of Users unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return hasattr(g.user, 'role') and g.user.role == 'admin'

class VehicleAdmin(ModelView):
    # Prevent administration of Users unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return hasattr(g.user, 'role') and g.user.role == 'admin'

class HomeFormAdmin(ModelView):
    column_display_pk = True
    #inline_models = (User,)
    # Prevent administration of Users unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return hasattr(g.user, 'role') and g.user.role == 'admin'

class VehicleFormAdmin(ModelView):
    column_searchable_list = ['primary_driver.full_name']
    column_display_pk = True
    column_list = (
        'primary_driver', 'vehicles', 'additional_drivers', 'college_grads', 
        'college_grads_details', 'medical_coverage', 'current_billing'
    )
    # Prevent administration of Users unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return hasattr(g.user, 'role') and g.user.role == 'admin'

admin.add_view(UserAdmin(User, db.session))
admin.add_view(DriverAdmin(Driver, db.session))
admin.add_view(VehicleFormAdmin(VehicleForm, db.session))
admin.add_view(VehicleAdmin(Vehicle, db.session))
admin.add_view(HomeFormAdmin(HomeForm, db.session))
