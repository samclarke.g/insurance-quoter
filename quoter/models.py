from . import db
import datetime
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy_utils import ChoiceType

vehicle_form_map = {
    'collegeGrads': 'college_grads',
    'collegeGradDetails': 'college_grads_details',
    #medicalCoverage = db.Column(db.Boolean, nullable=False, default=False)
    #currentBilling = db.Column(db.String(20))
}


class User(db.Model):
    """User model."""

    ROLES = [
        (u'user', u'User'),
        (u'admin', u'Admin'),
    ]

    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), index=True)
    password_hash = db.Column(db.String(128))
    email = db.Column(db.String(50), unique=True, index=True)
    registered_on = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    role = db.Column(ChoiceType(ROLES), default=u'user')

    def __repr__(self):
        return '<User %r>' % self.username

    def hash_password(self, password):
        """Model method."""
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        """Model method."""
        return pwd_context.verify(password, self.password_hash)

    def is_authenticated(self):
        """Model method."""
        return True

    def is_active(self):
        """Model method."""
        return True

    def is_anonymous(self):
        """Model method."""
        return False

    def get_id(self):
        """Model method."""
        return unicode(self.id)



driver_identifier = db.Table('driver_identifier',
    db.Column('vehicle_form_id', db.Integer, db.ForeignKey('vehicle_form.id')),
    db.Column('driver_id', db.Integer, db.ForeignKey('driver.id'))
)

vehicle_identifier = db.Table('vehicle_identifier',
    db.Column('vehicle_form_id', db.Integer, db.ForeignKey('vehicle_form.id')),
    db.Column('vehicle_id', db.Integer, db.ForeignKey('vehicle.id'))
)

class Driver(db.Model):

    __tablename__ = "driver"
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(50))
    drivers_license = db.Column(db.String(20))
    mailing_address = db.Column(db.String(200))
    date_of_birth = db.Column(db.String(20))
    tickets = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return '<Driver %r>' % self.full_name   


class Vehicle(db.Model):

    __tablename__ = "vehicle"
    id = db.Column(db.Integer, primary_key=True)
    vin = db.Column(db.String(50), nullable=False)
    year = db.Column(db.String(50), nullable=False)
    make = db.Column(db.String(50), nullable=False)
    model = db.Column(db.String(50), nullable=False)
    coverage = db.Column(db.String(50), nullable=False)
    use = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return '<Vehicle %r>' % ', '.join([self.year, self.make, self.model])


class VehicleForm(db.Model):

    user = db.relationship(
        'User',
        backref=db.backref('vehicle_form',
        lazy='dynamic')
    )
    primary_driver = db.relationship(
        'Driver',
        backref=db.backref('vehicle_form',
        lazy='dynamic')
    )
    #  vehicles (many-to-many)
    vehicles = db.relationship("Vehicle",
                               secondary=vehicle_identifier)
    #  additonal drivers (many-to-many)
    additional_drivers = db.relationship("Driver",
                               secondary=driver_identifier)

    __tablename__ = "vehicle_form"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    driver_id = db.Column(db.Integer, db.ForeignKey('driver.id'))
    college_grads = db.Column(db.Boolean, nullable=False, default=False)
    college_grads_details = db.Column(db.String(200))
    coverage_required = db.Column(db.String(200))
    use = db.Column(db.String(200))
    medical_coverage = db.Column(db.Boolean, nullable=False, default=False)
    current_billing = db.Column(db.String(20))


class HomeForm(db.Model):

    user = db.relationship(
        'User',
        backref=db.backref('home_form', lazy='dynamic')
    )

    __tablename__ = "home_form"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    full_name = db.Column(db.String(50))
    age = db.Column(db.Integer, nullable=True)
    basement_exists = db.Column(db.Boolean, nullable=False, default=False)
    basement_percent_finished = db.Column(db.Integer, nullable=False, default=0)
    recent_claims = db.Column(db.Boolean, nullable=False, default=False)
    recent_loss = db.Column(db.String(200))
    wood_stove = db.Column(db.Boolean, nullable=False, default=False)
    current_billing = db.Column(db.String(20))
