from flask_script import Manager
from quoter import app, db
from quoter.models import User


manager = Manager(app)


@manager.command
def createdb():
    """Create the db."""
    db.drop_all()
    db.create_all()


@manager.command
def create_user():
    """Create default user instance in db."""
    u = User(
        username=u'testing',
        email=u'testing@gmail.com',
        confirmed=True,
        role=u'admin'
    )
    u.hash_password('testing')
    db.session.add(u)
    db.session.commit()


if __name__ == '__main__':
    manager.run()
