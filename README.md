# Insurance Quoter (Flask + Vue)

## Official Docs
    http://flask.pocoo.org/
    
    https://vuejs.org/


## Backend Setup

``` bash
# create virtualenv and install dependencies
mkvirtualenv insurance-quoter
pip install -r requirements.txt

# run dev server
python runserver.py
```


## Frontend Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```


## Running in Production using Gunicorn

``` bash
# use a process manager such as Upstart or SystemD 
/path/to/virtualenv/bin/gunicorn --access-logfile - --workers 3 --bind unix:insurance-quoter.sock wsgi:app
```
